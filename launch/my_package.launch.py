import os
from ament_index_python.packages import get_package_share_directory
import launch
import launch_ros.actions

def generate_launch_description():

    config = os.path.join(
        get_package_share_directory('my_package'),
        'config',
        'my_node_params.yaml'
        )
    
    first_node = launch_ros.actions.Node(
        package="my_package",
        executable="my_node",
        name="first_node",
        parameters=[
                {'my_parameter': launch.substitutions.EnvironmentVariable('USER'),
                'greet_param': 'world'}
        ],
        output="screen")

    second_node = launch_ros.actions.Node(
        package="my_package",
        executable="my_node",
        name=[launch.substitutions.LaunchConfiguration('node_prefix'), 'second_node'],
        parameters=[
                {'my_parameter': launch.substitutions.EnvironmentVariable('USER')}
        ],
        output="screen")

    third_node = launch_ros.actions.Node(
        package="my_package",
        executable="my_node",
        name="third_node",
        parameters=[config],
        output="screen")

    fourth_node = launch_ros.actions.Node(
        package="my_package",
        executable="my_node",
        name="third_node",
        namespace="my_namespace",
        parameters=[config],
        output="screen")

    return launch.LaunchDescription([
        launch.actions.SetEnvironmentVariable('USER', 'demo'),
        launch.actions.DeclareLaunchArgument(
            'node_prefix',
            default_value=[launch.substitutions.EnvironmentVariable('USER'), '_user_'],
            description='Prefix for node names'),

        first_node,
        second_node,
        third_node,
        fourth_node        
    ])